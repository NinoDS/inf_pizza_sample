import java.util.ArrayList;

public class Pizza extends MenuItem {

    private final ArrayList<String> ingredients;

    public Pizza(String name, int basicPrice, String size, ArrayList<String> ingredients){
        super(name, basicPrice, size);
        this.ingredients = new ArrayList<>(ingredients);
        this.type = MenuItemType.PIZZA;
    }

    public Pizza(Pizza otherPizza) {
        super(otherPizza);
        this.ingredients = new ArrayList<>(otherPizza.ingredients);
    }

    public Pizza(Pizza otherPizza, String size) {
        super(otherPizza, size);
        this.ingredients = new ArrayList<>(otherPizza.ingredients);
    }

}