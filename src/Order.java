import java.util.ArrayList;
import java.util.Scanner;

public class Order {

    private final ArrayList<MenuItem> wishlist = new ArrayList<>();

    public void help() {
        Menu.printMenu();
        printOrder();
        deleteItemFromOrderOption();
    }

    private void printOrder() {
        if (wishlist.size() > 0) {
            System.out.println("Ihre Bestellung:");
            for (MenuItem item : this.wishlist) {
                System.out.println("    " + item);
            }
            System.out.println("------------------------------");
        }
    }

    private void deleteItemFromOrderOption() {
        if (wishlist.size() > 0) {
            System.out.println("Wollen Sie ein Item aus ihrer Bestellung streichen? (y/N)");
            Scanner sc = new Scanner(System.in);
            String ans = sc.nextLine();

            if (ans.equals("y") || ans.equals("Y")) {
                System.out.println("Welches Item wollen sie entfernen?");
                int itemIndexToDelete = sc.nextInt();

                if (itemIndexToDelete > wishlist.size()) System.out.println("Inkorrekte Artikelnummer");
                else wishlist.remove(itemIndexToDelete - 1);
            }
        }
    }

    private int getPriceOfTheOrder() {
        int currentPrice = 0;
        for (MenuItem item : wishlist) {
            currentPrice += item.getPrice();
        }
        return currentPrice;
    }

    private String priceToString(int price) {
        return String.format("%.2f", price / 100.0);
    }

    public void addItem(String input, String size) {
        MenuItem orderedItem = Menu.order(input, size);
        if (orderedItem == null) return;

        wishlist.add(orderedItem);

        System.out.println("Die Bestellsumme beträgt " + priceToString(getPriceOfTheOrder()) + " Euro.");
    }

}
