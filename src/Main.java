import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Order order = new Order();

        while (true) {
            System.out.println("Willkommen bei ihrem elektronischen Pizza-Bestellsystem. ");
            System.out.println("Geben Sie die Namen der zu bestellenden Pizzen nacheinander ein. Um hingegen alle aktuell bestellten Artikel anzuzeigen geben Sie ein Fragezeichen (?) ein.");
            Scanner sc = new Scanner(System.in);
            String input = sc.nextLine();

            if (input.equals("?")) {
                order.help();

            } else if (input.equals("exit")) {
                break;
            }
            else {
                System.out.println("Welche Größe soll die Pizza haben (mittel/groß/sehr groß)");
                String size = sc.nextLine();
                order.addItem(input, size);
            }
        }
    }
}