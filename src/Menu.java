import java.util.ArrayList;
import java.util.Arrays;

public class Menu {
    private static final ArrayList<MenuItem> availableItems = new ArrayList<>(Arrays.asList(
            new Pizza(
                    "Pizza Margherita",
                    580,
                     "mittel",
                     new ArrayList<>(Arrays.asList("Tomaten", "Mozzarella", "Basilikum"))
            ),
            new Pizza(
                    "Pizza Salami",
                    650,
                    "mittel",
                    new ArrayList<>(Arrays.asList("Tomaten", "Mozzarella", "scharfe Salami"))
            ),
            new Pizza(
                    "Pizza Prosciutto",
                    730,
                    "mittel",
                    new ArrayList<>(Arrays.asList("Tomaten", "Mozzarella", "Parmaschinken"))
            ),
            new Drink(
                    "Coca Cola",
                    200,
                    "mittel"
            ),
            new Drink(
                    "Apfelsaft",
                    150,
                    "mittel"
            )
    ));

    public static ArrayList<MenuItem> getItems() {
        return availableItems;
    }

    public static void printMenu() {
        System.out.println("------------------------------");
        for (MenuItem item : availableItems) {
            System.out.println(item);
        }
        System.out.println("------------------------------");
    }

    public static int findItem(String itemName) {
        for (int i = 0; i < availableItems.size(); ++i) {
            if (availableItems.get(i).getName().equals(itemName)) {
                return i;
            }
        }
        return -1;
    }

    public static MenuItem order(String itemNameOrdered, String size){
        int itemIndex = findItem(itemNameOrdered);
        if (itemIndex != -1) {
            return new MenuItem(availableItems.get(itemIndex), size);
        }

        System.out.println("Bestellfehler: Einen Artikel mit diesem Namen gibt es nicht.");
        return null;
    }
}
