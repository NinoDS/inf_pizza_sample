public class Drink extends MenuItem{

    public Drink(String name, int basic_price, String size){
        super(name, basic_price, size);
        this.type = MenuItemType.DRINK;
    }
}
