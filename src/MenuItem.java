import java.util.ArrayList;
import java.util.Arrays;

public class MenuItem {
    protected enum MenuItemType {
        PIZZA,
        DRINK
    }
    private final String name;
    private final String size;
    private final int basicPrice;
    private static final ArrayList<String> availableSizes = new ArrayList<>(Arrays.asList("mittel", "groß", "sehr groß"));

    protected MenuItemType type; // Pizza | Drink

    public MenuItem(String name, int basicPrice, String size){
        this.name = name;
        this.basicPrice = basicPrice;

        if (availableSizes.contains(size)) this.size = size;
        else this.size = "mittel";
    }

    public MenuItem(MenuItem otherItem) {
        this.name = otherItem.name;
        this.basicPrice = otherItem.basicPrice;
        this.size = otherItem.size;
        this.type = otherItem.type;
    }

    public MenuItem(MenuItem otherItem, String size) {
        this.name = otherItem.name;
        this.basicPrice = otherItem.basicPrice;

        if (availableSizes.contains(size)) this.size = size;
        else this.size = "mittel";

        this.type = otherItem.type;
    }

    public int getPrice(){
        return (int) (basicPrice * (1 + availableSizes.indexOf(size) / 10.0f) * 119 /100);
    }

    public String getName(){
        return name;
    }

    public String getSize() {
        return size;
    }

    public MenuItemType getType() {
        return type;
    }

    @Override
    public String toString() {
        String icon;
        if (type == MenuItemType.PIZZA) icon = "\uD83C\uDF55 ";
        else icon = "\uD83E\uDD64 ";
        return icon + name;
    }
}
